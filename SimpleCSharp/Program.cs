﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using SimpleCSharp.Exceptions;
using SimpleCSharp.QueryProcessor;
using SimpleCSharp.QueryProcessor.Exceptions;
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace SimpleCSharp
{
    class Program
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            var p = new Program();
            p.Main(args[0]);
        }

        public void Main(string filename)
        {
            var parser = new Parser.Parser();
            parser.SetCode(File.ReadAllText(filename));
            try
            {
                var result = parser.Parse();
                Logger.Debug($"validation result: {result != null}");

                var analyser = new Analyser.Analyser(result.Children[0]);
                analyser.AnalyzeTree();
                Logger.Debug("Analyze successful");

         
                string synonyms;
                string query;
                string resultOfQuery;
                QueryValidator qv;
                QueryEvaluator qe;
                QueryTree qt;
                QueryResultProjector qrp;
                Console.WriteLine("Ready");
                while (true)
                {
                    qv = new QueryValidator();
                    try
                    {
                        synonyms = Console.ReadLine();
                        if (synonyms.Equals("exit")) { break; }
                        query = Console.ReadLine();
                        if (qv.isSynonymAndQueryValid(synonyms, query))
                        {
                            qt = qv.queryTree;
                            qe = new QueryEvaluator(qt);
                            qe.EvaluateQuery();
                            qrp = new QueryResultProjector(qe.getRawResult(), qt.selectClause);
                            resultOfQuery = qrp.getCommaSeparatedResult();
                            Console.WriteLine(resultOfQuery);
                        }
                    }
                    catch (QueryInvalidException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    catch (SynonymsInvalidException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("# EXCEPTION: "+e.Message);
                    }
                }
                
                //var modifies = PKB.getModifies("assign", "t");
                //var temp1 = PKB.Nexts;
                //var temp2 = PKB.Parents;
                //var temp3 = PKB.Modifies;
                //var temp4 = PKB.Uses;
            } //break point tu w analyser są listy modifies, next, ...
            catch (ParserException ex)
            {
                Logger.Debug($"validation error: {ex.Message}");
            }
            catch (Exception ex)
            {
                Logger.Debug($"Analyze error: {ex.Message}");
            }
            //	Console.ReadKey();
        }
    }
}
