﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Exceptions
{
	public class StmtLstExpectedException : ParserException
	{
		public StmtLstExpectedException(int position) : base("Expected: StmtLstExpectedException on position: " + position)
		{
		}
	}
}
