﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Exceptions
{
    public class ExpectedException : ParserException
    {
        public ExpectedException(string text, int position) : base("Expected: " + text + " on position: " + position)//TODO: postion -> line
        {
        }
    }
}
