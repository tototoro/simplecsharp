﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Analyser
{
	public partial class Analyser
	{
		private void AnalyzeProcedure(Node procedure)
		{
			this.GetProcedureNexts(procedure);
			this.AnalyzeProcedureChildren(procedure);
		}

		private void GetProcedureNexts(Node procedure)
		{
			var firstChild = procedure.Children[0].Children[0];
			PKB.Nexts.Add(new Tuple<Element, Element>(new Element(procedure.Line, procedure.Name), new Element(firstChild.Line, firstChild.Name)));
		}

		private void GetFolowsForProcedureChild(Node parent, Node rootChild, int position)
		{
			if (position < parent.Children[0].Children.Count - 1)
			{
				Node secondRootChild = parent.Children[0].Children[position + 1];
				PKB.Follows.Add(new Tuple<Element, Element>(new Element(rootChild.Line, rootChild.Name), new Element(secondRootChild.Line, secondRootChild.Name)));
			}
		}

		private void AnalyzeProcedureChildren(Node procedure)
		{
			for (var position = 0; position < procedure.Children[0].Children.Count; position++)
			{
				Node rootChild = procedure.Children[0].Children[position];

				PKB.Parents.Add(new Tuple<Element, Element>(new Element(procedure.Line, procedure.Name), new Element(rootChild.Line, rootChild.Name)));
				if (rootChild.Name.Equals("assign"))
				{
					Logger.Debug("Line nr. " + rootChild.Line + " is Assignment");
					this.AnalyseAssign(procedure.Children[0], position);
				}
				else if (rootChild.Name.Equals("while"))
				{
					Logger.Debug("Line nr. " + rootChild.Line + " is While");
					this.AnalyseWhile(procedure.Children[0], position);
				}

				this.GetFolowsForProcedureChild(procedure, rootChild, position);
			}
		}
	}
}
