﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Analyser
{
	public partial class Analyser
	{
		/// <summary>
		/// Logger analizatora.
		/// </summary>
		private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(Analyser));

		/// <summary>
		/// Korzeń drzewa.
		/// </summary>
		private readonly Node root;

		public Analyser(Node root)
		{
			this.root = root;
		}

		public void AnalyzeTree()
		{
			this.AnalyzeProcedure(this.root);
		}
	}
}
