﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Analyser
{
	public partial class Analyser
	{
		private void AnalyseExp(Node parent, List<Node> exp)
		{
			foreach (var expChild in exp)
			{
				if (expChild.Name.Equals("factor"))
				{
					PKB.Uses.Add(new Tuple<Element, string>(new Element(parent.Line, parent.Name), ((FactorVal)expChild).Value));
				}
				else
				{
					this.AnalyseExp(parent, expChild.Children.ToList());
				}
			}
		}
	}
}
