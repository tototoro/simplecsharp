﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Analyser
{
	public partial class Analyser
	{
		private void AnalyseWhile(Node parent, int whilePosition)
		{
			var whileNode = parent.Children[whilePosition];

			this.GetWhileUses(whileNode);
			this.GetWhileNexts(parent, whileNode, whilePosition);
			this.AnalyseWhileChildren(whileNode);
		}

		private void GetWhileUses(Node whileNode)
		{
			PKB.Uses.Add(new Tuple<Element, string>(new Element(whileNode.Line, whileNode.Name), ((NameVal)whileNode.Children[0]).Value));
		}

		private void GetWhileNexts(Node parent, Node whileNode, int whilePosition)
		{
			var firsChild = whileNode.Children[1].Children[0];
			PKB.Nexts.Add(new Tuple<Element, Element>(new Element(whileNode.Line, whileNode.Name), new Element(firsChild.Line, firsChild.Name)));

			Node lastWhileChild = whileNode.Children[1].Children[whileNode.Children[1].Children.Count - 1];
			PKB.Nexts.Add(new Tuple<Element, Element>(new Element(lastWhileChild.Line, lastWhileChild.Name), new Element(whileNode.Line, whileNode.Name)));

			if (whilePosition < parent.Children.Count - 1)
			{
				var next = parent.Children[whilePosition + 1];
				PKB.Nexts.Add(new Tuple<Element, Element>(new Element(whileNode.Line, whileNode.Name), new Element(next.Line, next.Name)));
			}
		}

		private void GetFolowsForWhileChild(Node parent, Node rootChild, int position)
		{
			if (position < parent.Children[1].Children.Count - 1)
			{
				Node secondRootChild = parent.Children[1].Children[position + 1];
				PKB.Follows.Add(new Tuple<Element, Element>(new Element(rootChild.Line, rootChild.Name), new Element(secondRootChild.Line, secondRootChild.Name)));
			}
		}

		private void AnalyseWhileChildren(Node whileNode)
		{
			for (var position = 0; position < whileNode.Children[1].Children.Count; position++)
			{
				Node whileChild = whileNode.Children[1].Children[position];

				PKB.Parents.Add(new Tuple<Element, Element>(new Element(whileNode.Line, whileNode.Name), new Element(whileChild.Line, whileChild.Name)));
				if (whileChild.Name.Equals("assign"))
				{
					this.AnalyseAssign(whileNode.Children[1], position);
				}
				else if (whileChild.Name.Equals("while"))
				{
					this.AnalyseWhile(whileNode.Children[1], position);
				}

				this.GetFolowsForWhileChild(whileNode, whileChild, position);
			}
		}
	}
}
