﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace SimpleCSharp
{
    public class TextAnalyzer
    {
        public string Text { get; set; }
        public int Cursor { get; set; }

        public char this[int index] => this.Text[index];

        public bool TryRead(string text)
        {
            if (this.Text.IndexOf(text, this.Cursor, StringComparison.Ordinal) != this.Cursor)
                return false;

            this.Cursor += text.Length;
            return true;
        }

        public bool IsEndText()
        {
            return this.Cursor >= this.Text.Length;  // bo skoro kursor jest rowny dlugosci tekstu to sie skonczyl -- wojtek
        }

        internal string TryRead(Regex regex)
        {
            var aa = regex.Match(this.Text, this.Cursor);

            if (aa.Index != this.Cursor)
                return null;

            var result = aa.Value;
            this.Cursor += aa.Length;
            return result;
        }

        public int SkipEmptyChars()
        {
            while (this.Cursor < this.Text.Length && Char.IsWhiteSpace(this.Text[this.Cursor]))
            {
                this.Cursor++;
            }

            return this.Cursor;
        }
    }
}
