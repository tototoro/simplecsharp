﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    class Call : Stmt
    {
        public override string Name => "call";

        public string Value { get; set; }

        public Call()
        {
        }

        public Call(int line, int position) : base(line, position)
        {
        }
    }
}
