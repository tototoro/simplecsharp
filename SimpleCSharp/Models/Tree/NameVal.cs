﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    class NameVal : Node
    {
        public override string Name => "name";

        public string Value { get; set; }

        public NameVal() : base()
        {

        }

        public NameVal(int line, int position) : base(line, position)
        {

        }
    }
}
