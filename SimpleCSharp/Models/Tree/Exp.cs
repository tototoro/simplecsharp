﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    class Exp : FactorVal
    {
        public override string Name => "exp";

        public Exp() : base()
        {
        }

        public Exp(int line, int position) : base(line, position)
        {

        }
    }
}
