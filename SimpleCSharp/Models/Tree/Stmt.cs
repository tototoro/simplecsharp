﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    public abstract class Stmt : Node
    {
        protected Stmt() : base() { }
        protected Stmt(int line, int position) : base(line, position) { }
    }
}
