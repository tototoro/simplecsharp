﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    class Procedure : Node
    {
        public override string Name => "procedure";

        public string Value { get; internal set; }

        public Procedure() : base()
        {
        }

        public Procedure(int line, int position) : base(line, position)
        {

        }
    }
}
