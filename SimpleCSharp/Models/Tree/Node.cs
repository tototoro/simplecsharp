﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    public abstract class Node
    {
        public abstract string Name { get; }
        public int Line { get; protected set; }
        public int Position { get; protected set; }

        public IList<Node> Children { get; protected set; }

        protected Node()
        {
            this.Children = new List<Node>();
        }

        protected Node(int line, int position) : this()
        {
            Line = line;
            Position = position;
        }
    }
}
