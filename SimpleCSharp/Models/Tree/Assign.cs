﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    class Assign : Stmt
    {
        public override string Name => "assign";

        public NameVal NameVal
        {
            get => this.Children[0] as NameVal;
            set => this.Children[0] = value;
        }
        public Node Exp
        {
            get => this.Children[1] as NameVal;
            set => this.Children[1] = value;
        }

        public Assign()
        {
            this.Children.Add(null);
            this.Children.Add(null);
        }

        public Assign(int line, int position) : base(line, position)
        {
            this.Children.Add(null);
            this.Children.Add(null);
        }
    }
}
