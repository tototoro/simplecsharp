﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCSharp.Models.Tree
{
    class While : Stmt
    {
        public override string Name => "while";

        public While() : base()
        {
        }

        public While(int line, int position) : base(line, position)
        {

        }
    }
}
