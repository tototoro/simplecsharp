﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleCSharp.Analyser;

namespace SimpleCSharp.QueryProcessor
{


    class QueryEvaluator
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(QueryEvaluator));



        private const string MODIFIES_REL = "Modifies";
        private const string USES_REL = "Uses";
        private const string PARENT_REL = "Parent";
        private const string PARENT_STAR_REL = "Parent*";
        private const string FOLLOWS_REL = "Follows";
        private const string FOLLOWS_STAR_REL = "Follows*";
        private const string CALLS_REL = "Calls";
        private const string CALLS_STAR_REL = "Calls*";
        private const string NEXT_REL = "Next";
        private const string NEXT_STAR_REL = "Next*";
        private const string TRUE_STRING = "true";
        private const string FALSE_STRING = "false";
        private const string BOOLEAN_STRING = "BOOLEAN";
        private const string SELECT_TYPE = "Select";
        private const string ASSIGN_TYPE = "assign";
        private const string IF_TYPE = "if";
        private const string WHILE_TYPE = "while";
        private const string PROG_LINE_TYPE = "prog_line";
        private const string STMT_TYPE = "stmt";
        private const string VARIABLE_TYPE = "variable";
        private const string CONSTANT_TYPE = "constant";
        private const string PROCEDURE_TYPE = "procedure";
        private const string CALL_TYPE = "call";
        private const string STRING_TYPE = "string";
        private const string NUMBER_TYPE = "number";

        private QueryTree qt;
        private QueryRawResult rawResult;

        public QueryRawResult getRawResult() { return rawResult; }

        public QueryEvaluator(QueryTree _qt)
        {
            this.qt = _qt;
            rawResult = new QueryRawResult((from o in qt.synonymsList select o.var[0]).ToList());
        }

        public void EvaluateQuery()
        {
            for (int i = 0; i < qt.clausesList.Count; i++)
            {
                if (!processClause(qt.clausesList[i]))
                {
                    rawResult.isQueryTrue = false;
                    break;
                }
            }
        }

        private bool processClause(Clause clause)
        {
            return processSuchThatClause(clause);
        }

        private bool processSuchThatClause(Clause clause)
        {
            if (clause.relationship.Equals(PARENT_REL))
            {
                return processParent(clause);
            }
            else if (clause.relationship.Equals(PARENT_STAR_REL))
            {
                return processParentStar(clause);
            }
            else if (clause.relationship.Equals(NEXT_REL))
            {
                return processParentStar(clause);
            }
            else if (clause.relationship.Equals(NEXT_STAR_REL))
            {
                return processParentStar(clause);
            }
            return false;
        }

        private bool processParent(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetParents(Int32.Parse(arg1), Int32.Parse(arg2));
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetParents(Int32.Parse(arg1), arg2Type);
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg2, c.Item2.Line);
                    }
                    return res.Count > 0;
                }
            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetParents(arg1Type, Int32.Parse(arg2));
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg1, c.Item1.Line);
                    }
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetParents(arg1Type, arg2Type);
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                    return res.Count > 0;
                }
            }
        }

        private bool processParentStar(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetParents(Int32.Parse(arg1), Int32.Parse(arg2), true);
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetParents(Int32.Parse(arg1), arg2Type, true);
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg2, c.Item2.Line);
                    }
                    return res.Count > 0;
                }

            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetParents(arg1Type, Int32.Parse(arg2), true);
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg1, c.Item1.Line);
                    }
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetParents(arg1Type, arg2Type, true);
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                    return res.Count > 0;
                }
            }
        }
        private bool processNext(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetNexts(Int32.Parse(arg1), Int32.Parse(arg2));
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetNexts(Int32.Parse(arg1), arg2Type);
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg2, c.Item2.Line);
                    }
                    return res.Count > 0;
                }
            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetNexts(arg1Type, Int32.Parse(arg2));
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg1, c.Item1.Line);
                    }
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetNexts(arg1Type, arg2Type);
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                    return res.Count > 0;
                }
            }
        }
        private bool processNextStar(Clause clause)
        {
            string arg1 = clause.var[0];
            string arg1Type = clause.varType[0];
            string arg2 = clause.var[1];
            string arg2Type = clause.varType[1];
            List<Tuple<Element, Element>> res = new List<Tuple<Element, Element>>();
            if (arg1Type.Equals("number"))
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetNexts(Int32.Parse(arg1), Int32.Parse(arg2), true);
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetNexts(Int32.Parse(arg1), arg2Type, true);
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg2, c.Item2.Line);
                    }
                    return res.Count > 0;
                }
            }
            else
            {
                if (arg2Type.Equals("number"))
                {
                    res = PKB.GetNexts(arg1Type, Int32.Parse(arg2), true);
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg1, c.Item1.Line);
                    }
                    return res.Count > 0;
                }
                else
                {
                    res = PKB.GetNexts(arg1Type, arg2Type, true);
                    foreach (var c in res)
                    {
                        rawResult.addToResult(arg2, c.Item2.Line, arg1, c.Item1.Line);
                    }
                    return res.Count > 0;
                }
            }
        }
    }
}
