﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp.QueryProcessor.Exceptions
{
    class QueryInvalidException:QueryProcessorException
    {
        public QueryInvalidException() : base("# QueryInvalidException: Invalid query declaration")
        {
        }
    }
}
