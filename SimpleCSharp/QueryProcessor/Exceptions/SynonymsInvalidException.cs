﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp.QueryProcessor.Exceptions
{
    class SynonymsInvalidException : QueryProcessorException
    {
        public SynonymsInvalidException() : base("# SynonymsInvalidException: Invalid synonyms declaration")
        {
        }
    }
}
