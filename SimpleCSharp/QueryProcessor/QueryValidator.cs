﻿using System;

using SimpleCSharp.QueryProcessor.Exceptions;
using System.Collections.Generic;
using System.Text.RegularExpressions;
namespace SimpleCSharp.QueryProcessor
{

    public class QueryValidator
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(QueryValidator));

        const string NO_RELATIONSHIP = "";

        const string VARTYPE_VARIABLE = "variable";
        const string VARTYPE_STRING = "string";
        const string VARTYPE_CONSTANT = "constant";
        const string VARTYPE_ASSIGN = "assign";
        const string VARTYPE_WHILE = "while";
        const string VARTYPE_IF = "if";
        const string VARTYPE_PROCNAME = "procName";
        const string VARTYPE_VARNAME = "varName";
        const string VARTYPE_PROG_LINE = "prog_line";
        const string VARTYPE_NUMBER = "number";
        const string VARTYPE_BOOLEAN = "BOOLEAN";

        const string RELTYPE_SELECT = "Select";
        const string RELTYPE_SUCHTHAT = "such that";
        const string RELTYPE_WITH = "with";
        const string RELTYPE_PATTERN = "pattern";

        private static string[] relationships ={
            "Modifies",
            "Uses",
            "Calls",
            "Calls*",
            "Parent",
            "Parent*",
            "Follows",
            "Follows*",
            "Next",
            "Next*"
        };
        private static string[] designEntites = {
            "procedure",
            "stmt",
            "stmtLst",
            "while",
            "assign",
            "if",
            "call",
            "variable",
            "constant",
            "prog_line"
        };
        private Dictionary<string, string> varMap = new Dictionary<string, string>();
        private RelationshipTable relTable = new RelationshipTable();
        private PatternTable patTable = new PatternTable();


        public QueryTree queryTree { get; }

        public QueryValidator()
        {
            queryTree = new QueryTree();
        }

        public bool isSynonymAndQueryValid(string synonyms, string query)
        {
            if (!isSynonymValid(synonyms))
            {
                throw new SynonymsInvalidException();
            }
            if (!isQueryValid(query))
            {
                throw new QueryInvalidException();
            }
            return true;
        }

        private bool isSynonymValid(string synonyms)
        {
            string[] vars = synonyms.Trim().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            if (vars.Length < 1) { return false; }
            for (int i = 0; i < vars.Length; i++)
            {
                string[] syms = vars[i].Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);  
                if (syms.Length < 2) { return false; }
                string[] vNames = syms[1].Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (!isValidDesignEntity(syms[0])) { return false; }
                for (int j = 0; j < vNames.Length; j++)
                {
                    if (isVarNameExists(vNames[j]))
                    {
                        return false;
                    }
                    queryTree.InsertSynonym(vNames[j], syms[0]);
                    varMap.Add(vNames[j], syms[0]);
                }
            }
            return true;
        }
        private bool isQueryValid(string query)
        {
            string temp = "";
            string temp1 = "";
            string temp2 = "";
            TextAnalyzer textAn = new TextAnalyzer();
            textAn.Text = query;
            if (textAn.TryRead(RELTYPE_SELECT))
            {
                textAn.SkipEmptyChars();
                if (textAn.TryRead(VARTYPE_BOOLEAN))
                {
                    queryTree.InsertSelect(new List<string>() { NO_RELATIONSHIP }, new List<string>() { VARTYPE_BOOLEAN });
                }
                else
                {
                    temp = textAn.TryRead(new Regex(@"(\w)+")).Trim();
                    if (!isVarNameExists(temp)) { return false; }
                    queryTree.InsertSelect(new List<string>() { temp }, new List<string>() { varMap[temp] });
                    if (textAn.IsEndText()) { return true; }
                }
                textAn.SkipEmptyChars();
                while (!textAn.IsEndText())
                {
                    textAn.SkipEmptyChars();
                    if (textAn.TryRead(RELTYPE_SUCHTHAT))
                    {
                        textAn.SkipEmptyChars();
                        temp = textAn.TryRead(new Regex("[A-Z]{1}[a-z\\*]+[ ]?\\([a-zA-Z0-9\"]+,[a-zA-Z0-9\"]+\\)")).Trim();
                        if (temp.Equals("")) { return false; }
                        if (!parseRelationship(temp)) { return false; }
                    }
                    else if (textAn.TryRead(RELTYPE_PATTERN))
                    {
                        textAn.SkipEmptyChars();
                        //temp = textAn.TryRead(new Regex("[a-zA-Z0-9]+[ ]?\\([a-zA-Z0-9]+,[a-zA-Z0-9]+\\)")).Trim();
                        temp1 = textAn.TryRead(new Regex(@"(\w)+")).Trim();
                        textAn.SkipEmptyChars();
                        temp2 = textAn.TryRead(new Regex("\\([a-zA-Z0-9_\"]+,[a-zA-Z0-9_\"]+\\)")).Trim();
                        if (temp1.Equals("")){ return false; }
                        if (temp2.Equals("")){ return false; }
                        if (!parsePattern(temp1, temp2)) { return false; }
                    }
                    else if (textAn.TryRead(RELTYPE_WITH))
                    {
                        // todo - trzeba napisac metode -- 'parseRelationship(rel)' tego nie ogarnie
                        if (!parseWith()) { return false; }
                    }
                    else return false;
                }
            }
            else return false;
            return true;
        }

        private bool parsePattern(string s1, string s2)
        {
            TextAnalyzer tr = new TextAnalyzer();
            tr.Text = s1;
            string args, syn;
            string[] temp;
            syn = tr.TryRead(new Regex(@"(\w)+"));
            if (!isVarNameExists(syn)) { return false; }
            tr.Text = s2;
            tr.SkipEmptyChars();
            args = tr.TryRead(new Regex("\\([a-zA-Z0-9_\"]+,[a-zA-Z0-9_\"]+\\)")).Trim(new char[] { '(', ')' });
            temp = args.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (temp.Length != 2) { return false; }
            List<string> vars = new List<string>(temp);
            List<string> varTypes = new List<string>();

            for (int i = 0; i < vars.Count; i++)
            {
                vars[i] = vars[i].Trim();
                varTypes[i] = "string";
            }

            queryTree.InsertPattern("pattern", vars, varTypes);
            return true;
        }
        private bool parseWith()
        {
            //todo
            queryTree.InsertWith(); //todo
            return true;
        }

        private bool parseRelationship(string rel)
        {

            TextAnalyzer tr = new TextAnalyzer();
            tr.Text = rel;

            string args, relation;
            string[] temp;
            relation = tr.TryRead(new Regex("[A-Z]{1}[a-z\\*]+"));

            if (!isValidRelationship(relation)) { return false; }
            tr.SkipEmptyChars();
            args = tr.TryRead(new Regex("\\([a-zA-Z0-9]+,[a-zA-Z0-9]+\\)")).Trim(new char[] { '(', ')' });
            temp = args.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (temp.Length != 2) { return false; }
            List<string> vars = new List<string>(temp);
            List<string> varTypes = new List<string>();
            int tmp;
            for (int i = 0; i < vars.Count; i++)
            {
                vars[i] = vars[i].Trim();
                if (isVarNameExists(vars[i]))
                {
                    if (!relTable.isArgValid(relation, i + 1, getVarType(vars[i])))
                    {
                        return false;
                    }
                    varTypes.Add(getVarType(vars[i]));
                }
                else if (vars[i].Length > 2 && vars[0].Equals("\"") && vars[vars[i].Length - 1].Equals("\""))
                {
                    if (!relTable.isArgValid(relation, i + 1, VARTYPE_STRING)) { return false; }
                    vars[i] = vars[i].Substring(1, vars[i].Length - 2);
                    varTypes.Add(VARTYPE_STRING);
                }
                else if (Int32.TryParse(vars[i], out tmp))
                {
                    if (!relTable.isArgValid(relation, i + 1, VARTYPE_NUMBER)) { return false; }
                    varTypes.Add(VARTYPE_NUMBER);
                }
            }
            queryTree.InsertSuchThat(relation, vars, varTypes);
            return true;
        }


        private string getVarType(string var)
        {
            return varMap[var];
        }

        private bool isVarNameExists(string varName)
        {
            return varMap.ContainsKey(varName);
        }

        private bool isValidDesignEntity(string entity)
        {
            for (int i = 0; i < designEntites.Length; i++)
            {
                if (designEntites[i].Equals(entity))
                {
                    return true;
                }
            }
            return false;
        }

        private bool isValidRelationship(string rel)
        {
            for (int i = 0; i < relationships.Length; i++)
            {
                if (relationships[i].Equals(rel))
                {
                    return true;
                }
            }
            return false;
        }
    }
}