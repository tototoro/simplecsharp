﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp.QueryProcessor
{
    class RelationshipTable
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(RelationshipTable));
        public Dictionary<string, Relationship> relTable = new Dictionary<string, Relationship>();
        public RelationshipTable()
        {
            List<string> arg1;
            List<string> arg2;

            //Modifies
            arg1 = new List<string>() { "stmt", "assign", "while", "prog_line", "if", "call", "procedure", "string", "number" };
            arg2 = new List<string>() { "variable", "string", "all" };
            Relationship r1 = new Relationship(2, arg1, arg2);
            relTable["Modifies"] = r1;

            //Uses
            arg1 = new List<string>() { "stmt", "assign", "while", "prog_line", "if", "call", "procedure", "string", "number" };
            arg2 = new List<string>() { "variable", "string", "all" };
            Relationship r2 = new Relationship(2, arg1, arg2);
            relTable["Uses"] = r2;

            //Parent
            arg1 = new List<string>() { "stmt", "prog_line", "while", "if", "number", "all" };
            arg2 = new List<string>() { "stmt", "assign", "prog_line", "while", "if", "call", "number", "all" };
            Relationship r3 = new Relationship(2, arg1, arg2);
            relTable["Parent"] = r3;

            //Parent*
            arg1 = new List<string>() { "stmt", "prog_line", "while", "if", "number", "all" };
            arg2 = new List<string>() { "stmt", "assign", "prog_line", "while", "if", "call", "number", "all" };
            Relationship r4 = new Relationship(2, arg1, arg2);
            relTable["Parent*"] = r4;

            //Follows
            arg1 = new List<string>() { "stmt", "assign", "prog_line", "while", "if", "call", "number", "all" };
            arg2 = new List<string>() { "stmt", "assign", "prog_line", "while", "if", "call", "number", "all" };
            Relationship r5 = new Relationship(2, arg1, arg2);
            relTable["Follows"] = r5;

            //Follows*
            arg1 = new List<string>() { "stmt", "assign", "prog_line", "while", "if", "call", "number", "all" };
            arg2 = new List<string>() { "stmt", "assign", "prog_line", "while", "if", "call", "number", "all" };
            Relationship r6 = new Relationship(2, arg1, arg2);
            relTable["Follows*"] = r6;

            //Pattern-assign
            arg1 = new List<string>() { "variable", "string", "all" };
            arg2 = new List<string>() { "substring", "string", "all" };
            Relationship r7 = new Relationship(2, arg1, arg2);
            relTable["PatternAssign"] = r7;

            //Pattern-if
            arg1 = new List<string>() { "variable", "string", "all" };
            arg2 = new List<string>() { "all" };
            Relationship r8 = new Relationship(3, arg1, arg2);
            relTable["PatternIf"] = r8;

            //Pattern-while
            arg1 = new List<string>() { "variable", "string", "all" };
            arg2 = new List<string>() { "all" };
            Relationship r9 = new Relationship(2, arg1, arg2);
            relTable["PatternWhile"] = r9;

            //Next
            arg1 = new List<string>() { "stmt", "assign", "while", "if", "call", "prog_line", "number", "all" };
            arg2 = new List<string>() { "stmt", "assign", "while", "if", "call", "prog_line", "number", "all" };
            Relationship r10 = new Relationship(2, arg1, arg2);
            relTable["Next"] = r10;

            //Next*
            arg1 = new List<string>() { "stmt", "assign", "while", "if", "call", "prog_line", "number", "all" };
            arg2 = new List<string>() { "stmt", "assign", "while", "if", "call", "prog_line", "number", "all" };
            Relationship r11 = new Relationship(2, arg1, arg2);
            relTable["Next*"] = r11;
        
            //Calls
            arg1 = new List<string>() { "procedure", "string", "all" };
            arg2 = new List<string>() { "procedure", "string", "all" };
            Relationship r12 = new Relationship(2, arg1, arg2);
            relTable["Calls"] = r12;
         
            //Calls*
            arg1 = new List<string>() { "procedure", "string", "all" };
            arg2 = new List<string>() { "procedure", "string", "all" };
            Relationship r13 = new Relationship(2, arg1, arg2);
            relTable["Calls*"] = r13;

            //WithNumber
            arg1 = new List<string>() { "constant", "stmt", "assign", "while", "if", "call", "number", "prog_line" };
            arg2 = new List<string>() { "constant", "stmt", "assign", "while", "if", "call", "number", "prog_line" };
            Relationship r14 = new Relationship(2, arg1, arg2);
            relTable["WithNumber"] = r14;

            //WithName
            arg1 = new List<string>() { "procedure", "call", "variable", "string" };
            arg2 = new List<string>() { "procedure", "call", "variable", "string" };
            Relationship r15 = new Relationship(2, arg1, arg2);
            relTable["WithName"] = r15;
        }

        public bool isArgValid(string rel,int argNo,string dataType)
        {
            switch (argNo)
            {
                case 1:return isArg1Valid(rel, dataType);break;
                case 2:return isArg2Valid(rel, dataType);break;
            }
            return false;
        }

        private bool isArg1Valid(string rel, string dataType)
        {
            foreach (var s in relTable[rel].args1)
            {
                if (s.Equals(dataType)) { return true; }
            }
            return false;
        }

        private bool isArg2Valid(string rel, string dataType)
        {
            foreach (var s in relTable[rel].args2)
            {
                if (s.Equals(dataType)) { return true; }
            }
            return false;
        }

    }
}

