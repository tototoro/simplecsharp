﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp.QueryProcessor
{

    class QueryRawResult
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(QueryRawResult));

        public bool isQueryTrue { get; set; } = true;
        public List<string> header { get; private set; } = new List<string>();
        public List<int[]> result { get; private set; } = new List<int[]>();

        public QueryRawResult(List<string> _header)
        {
            this.header = _header;
        }

        public void addToResult(string var, int line)
        {
            int[] t = new int[header.Count];
            t[IndexOfVar(var)]= line;
            result.Add(t);
        }
        public void addToResult(string var, int line, string var2, int line2)
        {
            int[] t = new int[header.Count];
            t[IndexOfVar(var)] = line;
            t[IndexOfVar(var2)] = line2;
            result.Add(t);
        }

        private int IndexOfVar(string var)
        {
            for (int i = 0; i < header.Count; i++)
            {
                if (header[i].Equals(var))
                {
                    return i;
                }
            }
            return -1;
        }


    }
}
