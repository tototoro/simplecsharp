﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Exceptions;
using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Parser
{
    public partial class Parser
    {
        private Procedure GetProcedure()
        {
            this.textAnalyzer.SkipEmptyChars();
            var position = this.textAnalyzer.Cursor;
            var line = this.GetLine(position);

            bool procedureWord = this.textAnalyzer.TryRead("procedure");
            if (!procedureWord)
            {
                //no cursor movement, no rollback
                return null;
            }

            string name = this.GetName();
            if (name == null)
            {
                throw new NotCorrectNameException(this.textAnalyzer.Cursor);
            }
            Logger.Debug($"procedure name: {name}");

            this.textAnalyzer.SkipEmptyChars();
            bool boChar = this.textAnalyzer.TryRead("{");
            if (!boChar)
            {
                throw new ExpectedException("{", this.textAnalyzer.Cursor);
            }

            var stmtLst = this.GetStmtLst();
            if (stmtLst == null)
            {
                throw new StmtLstExpectedException(this.textAnalyzer.Cursor);
            }

            this.textAnalyzer.SkipEmptyChars();
            bool bcChar = this.textAnalyzer.TryRead("}");
            if (!bcChar)
            {
                throw new ExpectedException("}", this.textAnalyzer.Cursor);
            }

            var procedure = new Procedure(line, position)
            {
                Value = name
            };
            procedure.Children.Add(stmtLst);
            return procedure;
        }
    }
}
