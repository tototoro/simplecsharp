﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Exceptions;
using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Parser
{
    public partial class Parser
    {
        private Call GetCall()
        {
            this.textAnalyzer.SkipEmptyChars();
            var position = this.textAnalyzer.Cursor;
            var line = this.GetLine(position);

            bool callWord = this.textAnalyzer.TryRead("call");
            if (!callWord)
            {
                //no cursor movement, no rollback
                return null;
            }

            string name = this.GetName();
            if (name == null)
            {
                throw new NotCorrectNameException(this.textAnalyzer.Cursor);
            }

            this.textAnalyzer.SkipEmptyChars();
            bool endChar = this.textAnalyzer.TryRead(";");
            if (!endChar)
            {
                throw new ExpectedException(";", this.textAnalyzer.Cursor);
            }

            var call = new Call(line, position)
            {
                Value = name
            };
            return call;
        }
    }
}
