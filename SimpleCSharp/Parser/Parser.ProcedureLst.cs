﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Exceptions;
using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Parser
{
    public partial class Parser
    {
        private ProcedureLst GetProcedureLst()
        {
            var procedure = this.GetProcedure();
            if (procedure == null)
                return null;

            var lst = new ProcedureLst(procedure.Line, procedure.Position);
            lst.Children.Add(procedure);

            while ((procedure = this.GetProcedure()) != null)
            {
                lst.Children.Add(procedure);
            }

            return lst;
        }
    }
}
