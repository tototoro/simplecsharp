﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Exceptions;
using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Parser
{
    public partial class Parser
    {
        private While GetWhile()
        {
            this.textAnalyzer.SkipEmptyChars();
            var position = this.textAnalyzer.Cursor;
            var line = this.GetLine(position);

            bool whileWord = this.textAnalyzer.TryRead("while");
            if (!whileWord)
            {
                //no cursor movement, no rollback
                return null;
            }

            string name = this.GetName();
            if (name == null)
            {
                throw new NotCorrectNameException(this.textAnalyzer.Cursor);
            }
            Logger.Debug($"while var: {name}");

            this.textAnalyzer.SkipEmptyChars();
            bool boChar = this.textAnalyzer.TryRead("{");
            if (!boChar)
            {
                throw new ExpectedException("{", this.textAnalyzer.Cursor);
            }

            var stmtLst = this.GetStmtLst();
            if (stmtLst == null)
            {
                throw new StmtLstExpectedException(this.textAnalyzer.Cursor);
            }

            this.textAnalyzer.SkipEmptyChars();
            bool bcChar = this.textAnalyzer.TryRead("}");
            if (!bcChar)
            {
                throw new ExpectedException("}", this.textAnalyzer.Cursor);
            }

            var whil = new While(line, position);
            whil.Children.Add(new NameVal(line, position)
            {
                Value = name
            });
            whil.Children.Add(stmtLst);//fixme
            return whil;
        }
    }
}
