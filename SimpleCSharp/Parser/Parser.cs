﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Parser
{
    public partial class Parser
    {
        private readonly TextAnalyzer textAnalyzer = new TextAnalyzer();
        private readonly Dictionary<int, int> linePosition = new Dictionary<int, int>();
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(Parser));

        public void SetCode(string code)
        {
            this.textAnalyzer.Text = code;

            var line = 1;
            var pos = 0;
            this.linePosition.Add(line++, pos);
            while ((pos = code.IndexOf('\n', pos + 1)) != -1)
            {
                this.linePosition.Add(line++, pos);
            }
        }

        private int GetLine(int position)
        {
            for (var i = 1; i < this.linePosition.Count; i++)
            {
                if (this.linePosition[i] <= position && this.linePosition[i + 1] > position)
                    return i;
            }

            return this.linePosition.Count;
        }


        public Node Parse()
        {
            var getProcedure = this.GetProcedureLst();

            return getProcedure;
        }

        protected string GetName()
        {
            this.textAnalyzer.SkipEmptyChars();
            return this.textAnalyzer.TryRead(new Regex("([a-zA-Z][a-zA-Z0-9]*)"));
        }

        protected int? GetNumber()
        {
            this.textAnalyzer.SkipEmptyChars();
            string result = this.textAnalyzer.TryRead(new Regex("([0-9]+)"));

            if (result == null) return null;
            return Int32.Parse(result);
        }
    }
}
