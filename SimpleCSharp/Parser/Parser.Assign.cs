﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Exceptions;
using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Parser
{
    public partial class Parser
    {
        private Assign GetAssign()
        {
            var cursor = this.textAnalyzer.Cursor;//to rollback

            this.textAnalyzer.SkipEmptyChars();
            var position = this.textAnalyzer.Cursor;
            var line = this.GetLine(position);

            string name = this.GetName();
            if (name == null)
            {
                //no cursor movement, no rollback
                return null;
            }
            Logger.Debug($"var name: {name}");

            this.textAnalyzer.SkipEmptyChars();
            bool eqChar = this.textAnalyzer.TryRead("=");
            if (!eqChar)
            {
                this.textAnalyzer.Cursor = cursor;
                return null;
            }

            Node exp = this.GetExp();

            if (exp == null)
            {
                throw new InvalidExpressionException(this.textAnalyzer.Cursor);
            }

            this.textAnalyzer.SkipEmptyChars();
            bool endChar = this.textAnalyzer.TryRead(";");
            if (!endChar)
            {
                throw new ExpectedException(";", this.textAnalyzer.Cursor);
            }

            var assign = new Assign(line, position)
            {
                NameVal = new NameVal(line, position)
                {
                    Value = name
                },
                Exp = exp
            };
            return assign;
        }
    }
}
