﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Parser
{
    public partial class Parser
    {
        private FactorVal GetExp()
        {
            var cursor = this.textAnalyzer.Cursor;//to rollback

            var position = this.textAnalyzer.Cursor;
            var line = this.GetLine(position);

            int? number = null;
            string name = this.GetName();
            if (name == null)
            {
                number = this.GetNumber();
                if (number == null)
                {
                    this.textAnalyzer.Cursor = cursor;
                    return null;
                }
            }
            var factorVal = new FactorVal(line, position)
            {
                Value = name ?? number.Value.ToString()
            };

            this.textAnalyzer.SkipEmptyChars();

            var operators = new string[] { "+", "*", "-", "/" };//TODO: move to const
            string oper = null;
            foreach (var op in operators)
            {
                if (!this.textAnalyzer.TryRead(op))
                {
                    continue;
                }

                oper = op;
                break;
            }

            if (oper == null)//no known mathematical operator was found
            {
                return factorVal;
            }


            var subExp = this.GetExp();
            if (subExp == null)
            {
                //or throw
                this.textAnalyzer.Cursor = cursor;
                return null;
            }

            //high priority
            if ((oper == "*" || oper == "/") && subExp.Name == "exp")
            {
                //subexpression
                var leftChild = subExp.Children[0];
                var newChildExp = new Exp(line, position)
                {
                    Value = oper
                };
                newChildExp.Children.Add(factorVal);
                newChildExp.Children.Add(leftChild);
                subExp.Children[0] = newChildExp;
                return subExp;
            }

            var exp = new Exp(line, position)
            {
                Value = oper
            };
            exp.Children.Add(factorVal);
            exp.Children.Add(subExp);
            return exp;
        }
    }
}
