﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleCSharp.Models.Tree;

namespace SimpleCSharp.Parser
{
	public partial class Parser
	{
		private Stmt GetStmt()
		{
			Stmt stmt = this.GetWhile();
			if (stmt != null)
				return stmt;

			stmt = this.GetIf();
			if (stmt != null)
				return stmt;

			stmt = this.GetCall();
			if (stmt != null)
				return stmt;

			stmt = this.GetAssign();
			return stmt;
		}
	}
}
