﻿using SimpleCSharp.Analyser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCSharp
{
    public static class PKB
    {
        /// <summary>
        /// Lista Next
        /// Pierwszy to linia po której następuje i co to jest
        /// Drugi to linia która następuje po pierwszej i co to jest
        /// </summary>
        public static List<Tuple<Element, Element>> Nexts = new List<Tuple<Element, Element>>();

        /// <summary>
        /// Lista Parent
        /// Pierwszy to rodzic i co to jest
        /// Drugi to dziecko i co to jest
        /// </summary>
        public static List<Tuple<Element, Element>> Parents = new List<Tuple<Element, Element>>();

        /// <summary>
        /// Lista Modifies
        /// Pierwszy to linia w której zachodzi modyfikacja i co to jest
        /// Drugi to element modyfikowany
        /// </summary>
        public static List<Tuple<Element, string>> Modifies = new List<Tuple<Element, string>>();

        /// <summary>
        /// Lista Uses
        /// Pierwszy to linia w której zachodzi użycie i co to jest
        /// Drugi to element wykorzystywany
        /// </summary>
        public static List<Tuple<Element, string>> Uses = new List<Tuple<Element, string>>();

        /// <summary>
        /// Lista Folows
        /// Pierwszy to rodzic i co to jest
        /// Drugi to dziecko i co to jest
        /// </summary>
        public static List<Tuple<Element, Element>> Follows = new List<Tuple<Element, Element>>();

        #region getNexts
        public static List<Tuple<Element, Element>> GetNexts(string stmt1, string stmt2, bool indirect = false)
        {
            List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

            if (stmt1.Equals("stmt") && stmt2.Equals("stmt"))
                return Nexts;

            if (!indirect)
            {
                if (stmt1.Equals("stmt"))
                    outList = Nexts.Where(x => x.Item2.Name == stmt2).ToList();
                else if (stmt2.Equals("stmt"))
                    outList = Nexts.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
                else
                    outList = Nexts.Where(x => x.Item1.Name.Equals(stmt1) && x.Item1.Name.Equals(stmt2)).ToList();
                return outList;
            }

            if (stmt1.Equals("stmt"))
                outList = Nexts.Where(x => x.Item2.Name == stmt2).ToList();
            else if (stmt2.Equals("stmt"))
                outList = Nexts.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
            else
                outList = Nexts.Where(x => x.Item1.Name.Equals(stmt1) && x.Item1.Name.Equals(stmt2)).ToList();

            var tempList = outList.ToList();
            var tempList2 = new List<Tuple<Element, Element>>();
            while (tempList.Count > 0 || tempList2.Count > 0)
            {
                foreach (var tuple in tempList)
                {
                    var temp = Nexts.Where(n => n.Item1.Name == tuple.Item2.Name && n.Item2.Name == stmt2).ToList();
                    outList.AddRange(temp);
                    tempList2.AddRange(temp);
                }
                tempList.Clear();

                foreach (var tuple in tempList2)
                {
                    var temp = Nexts.Where(n => n.Item1.Name == tuple.Item2.Name && n.Item2.Name == stmt2).ToList();
                    outList.AddRange(temp);
                    tempList.AddRange(temp);
                }
                tempList2.Clear();
            }

            return outList;
        }

        public static List<Tuple<Element, Element>> GetNexts(string stmt, int line, bool indirect = false)
        {
            List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

            if (!indirect)
            {
                if (stmt.Equals("stmt"))
                    outList = Nexts.Where(x => x.Item2.Line == line).ToList();
                else
                    outList = Nexts.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();
                return outList;
            }

            if (stmt.Equals("stmt"))
                outList = Nexts.Where(x => x.Item2.Line == line).ToList();
            else
                outList = Nexts.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();

            var tempList = outList.ToList();
            var tempList2 = new List<Tuple<Element, Element>>();
            while (tempList.Count > 0 || tempList2.Count > 0)
            {
                foreach (var tuple in tempList)
                {
                    var temp = Nexts.Where(n => n.Item1.Name == tuple.Item2.Name && n.Item2.Line == line).ToList();
                    outList.AddRange(temp);
                    tempList2.AddRange(temp);
                }
                tempList.Clear();

                foreach (var tuple in tempList2)
                {
                    var temp = Nexts.Where(n => n.Item1.Name == tuple.Item2.Name && n.Item2.Line == line).ToList();
                    outList.AddRange(temp);
                    tempList.AddRange(temp);
                }
                tempList2.Clear();
            }

            return outList;
        }

        public static List<Tuple<Element, Element>> GetNexts(int line, string stmt, bool indirect = false)
        {
            List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

            if (!indirect)
            {
                if (stmt.Equals("stmt"))
                    outList = Nexts.Where(x => x.Item1.Line == line).ToList();
                else
                    outList = Nexts.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();
                return outList;
            }

            if (stmt.Equals("stmt"))
                outList = Nexts.Where(x => x.Item1.Line == line).ToList();
            else
                outList = Nexts.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();

            var tempList = outList.ToList();
            var tempList2 = new List<Tuple<Element, Element>>();
            while (tempList.Count > 0 || tempList2.Count > 0)
            {
                foreach (var tuple in tempList)
                {
                    var temp = Nexts.Where(n => n.Item1.Line == tuple.Item2.Line && n.Item2.Name == stmt).ToList();
                    outList.AddRange(temp);
                    tempList2.AddRange(temp);
                }
                tempList.Clear();

                foreach (var tuple in tempList2)
                {
                    var temp = Nexts.Where(n => n.Item1.Line == tuple.Item2.Line && n.Item2.Name == stmt).ToList();
                    outList.AddRange(temp);
                    tempList.AddRange(temp);
                }
                tempList2.Clear();
            }

            return outList;
        }

        public static List<Tuple<Element, Element>> GetNexts(int line1, int line2, bool indirect = false)
        {
            return null; // TODO
        }
        #endregion

        #region getParents

        public static List<Tuple<Element, Element>> GetParents(int line, string stmt, bool indirect = false)
        {
            List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

            if (!indirect)
            {
                if (stmt.Equals("stmt"))
                    outList = Parents.Where(x => x.Item1.Line == line).ToList();
                else
                    outList = Parents.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();
                return outList;
            }

            if (stmt.Equals("stmt"))
                outList = Parents.Where(x => x.Item1.Line == line).ToList();
            else
                outList = Parents.Where(x => x.Item1.Line == line && x.Item2.Name == stmt).ToList();

            var tempList = outList.ToList();
            var tempList2 = new List<Tuple<Element, Element>>();
            while (tempList.Count > 0 || tempList2.Count > 0)
            {
                foreach (var tuple in tempList)
                {
                    var temp = Parents.Where(n => n.Item1.Line == tuple.Item2.Line && n.Item2.Name == stmt).ToList();
                    outList.AddRange(temp);
                    tempList2.AddRange(temp);
                }
                tempList.Clear();

                foreach (var tuple in tempList2)
                {
                    var temp = Parents.Where(n => n.Item1.Line == tuple.Item2.Line && n.Item2.Name == stmt).ToList();
                    outList.AddRange(temp);
                    tempList.AddRange(temp);
                }
                tempList2.Clear();
            }

            return outList;
        }

        public static List<Tuple<Element, Element>> GetParents(string stmt, int line, bool indirect = false)
        {
            List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

            if (!indirect)
            {
                if (stmt.Equals("stmt"))
                    outList = Parents.Where(x => x.Item2.Line == line).ToList();
                else
                    outList = Parents.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();
                return outList;
            }

            if (stmt.Equals("stmt"))
                outList = Parents.Where(x => x.Item2.Line == line).ToList();
            else
                outList = Parents.Where(x => x.Item1.Name == stmt && x.Item2.Line == line).ToList();

            var tempList = outList.ToList();
            var tempList2 = new List<Tuple<Element, Element>>();
            while (tempList.Count > 0 || tempList2.Count > 0)
            {
                foreach (var tuple in tempList)
                {
                    var temp = Parents.Where(n => (n.Item1.Name == stmt || stmt == "stmt") && n.Item2.Line == tuple.Item1.Line).ToList();
                    outList.AddRange(temp);
                    tempList2.AddRange(temp);
                }
                tempList.Clear();

                foreach (var tuple in tempList2)
                {
                    var temp = Parents.Where(n => (n.Item1.Name == stmt || stmt == "stmt") && n.Item2.Line == tuple.Item1.Line).ToList();
                    outList.AddRange(temp);
                    tempList.AddRange(temp);
                }
                tempList2.Clear();
            }

            return outList;
        }

        public static List<Tuple<Element, Element>> GetParents(string stmt1, string stmt2, bool indirect = false)
        {
            List<Tuple<Element, Element>> outList = new List<Tuple<Element, Element>>();

            if (stmt1.Equals("stmt") && stmt2.Equals("stmt"))
                return Parents;

            if (!indirect)
            {
                if (stmt1.Equals("stmt"))
                    outList = Parents.Where(x => x.Item2.Name == stmt2).ToList();
                else if (stmt2.Equals("stmt"))
                    outList = Parents.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
                else
                    outList = Parents.Where(x => x.Item1.Name.Equals(stmt1) && x.Item1.Name.Equals(stmt2)).ToList();
                return outList;
            }

            if (stmt1.Equals("stmt"))
                outList = Parents.Where(x => x.Item2.Name == stmt2).ToList();
            else if (stmt2.Equals("stmt"))
                outList = Parents.Where(x => x.Item1.Name.Equals(stmt1)).ToList();
            else
                outList = Parents.Where(x => x.Item1.Name.Equals(stmt1) && x.Item1.Name.Equals(stmt2)).ToList();

            var tempList = outList.ToList();
            var tempList2 = new List<Tuple<Element, Element>>();
            while (tempList.Count > 0 || tempList2.Count > 0)
            {
                foreach (var tuple in tempList)
                {
                    var temp = Parents.Where(n => n.Item1.Name == tuple.Item2.Name && n.Item2.Name == stmt2).ToList();
                    outList.AddRange(temp);
                    tempList2.AddRange(temp);
                }
                tempList.Clear();

                foreach (var tuple in tempList2)
                {
                    var temp = Parents.Where(n => n.Item1.Name == tuple.Item2.Name && n.Item2.Name == stmt2).ToList();
                    outList.AddRange(temp);
                    tempList.AddRange(temp);
                }
                tempList2.Clear();
            }

            return outList;
        }

        public static List<Tuple<Element, Element>> GetParents(int line1, int line2, bool indirect = false)
        {
            return null; // TODO
        }

        #endregion

        #region getModifies

        public static bool getModifies(string stmt, string var)
        {
            List<Tuple<Element, string>> outList = new List<Tuple<Element, string>>();

            if (stmt.Equals("stmt"))
                outList = Modifies.Where(x => x.Item2 == var).ToList();
            else
                outList = Modifies.Where(x => x.Item1.Name.Equals(stmt) && x.Item2 == var).ToList();

            return outList.Count > 0 ? true : false;
        }

        public static bool getModifies(int line, string var)
        {
            return Modifies.Where(x => x.Item1.Line == line && x.Item2 == var).Select(n => n.Item2).ToList().Count > 0 ? true : false;
        }

        #endregion

        #region getUses

        public static bool getUses(string stmt, string var)
        {
            List<Tuple<Element, string>> outList = new List<Tuple<Element, string>>();

            if (stmt.Equals("stmt"))
                outList = Uses.Where(x => x.Item2 == var).ToList();
            else
                outList = Uses.Where(x => x.Item1.Name.Equals(stmt) && x.Item2 == var).ToList();

            return outList.Count > 0 ? true : false;
        }

        public static bool getUses(int line, string var)
        {
            return Uses.Where(x => x.Item1.Line == line && x.Item2 == var).Select(n => n.Item2).ToList().Count > 0 ? true : false;
        }

        #endregion
    }
}
